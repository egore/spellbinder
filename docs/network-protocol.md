
# Packets from server to client

## 0x1: Player update

## 0x2: F_CREATE_OBJECT

This packet is sent to inform a client about a new game object.

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | velocity       |                |         |
| 2     | owner          |                |         |
| 2     | x              |                |         |
| 2     | y              |                |         |
| 2     | z              |                |         |
| 2     | id             | debug log      |         |
| 2     | casterPlayerId |                |         |
| 2     | which          | debug log      | If *type* is<br>**Projectile** or **Unknown3**: the index in the spell array<br>**Mongen**, **Static object** or **Unknown5**: the value is used as olist |
| 1     | z_velocity     |                |         |
| 1     | type           | debug log      | See [object types](#object-type) |
| 1     | team           |                | See [teams](#team)               |
| 1     | duration       |                | In seconds                       |

## 0x6: F_PLAYER_DEATH

Sent to inform the client a player was killed.

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | sourcePlayerId |                |         |
| 2     | targetPlayerId |                |         |
| 2     | ?              |                |         |

## 0x8: F_OBJECT_STATE

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | ? |                |         |
| 2     | ? |                |         |
| 2     | x |                |         |
| 2     | y |                |         |
| 2     | z |                |         |
| 2     | ? |                |         |

## 0xa: F_OBJECT_DEATH

Will remove a game object from the OLIST.

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | id             | debug log      | If the game object was a rune, it triggers additional logic (e.g. applying effects) |
| 2     | killer         | debug log      | Player that destroyed the object |

## 0xc: Create thin

Create a thin (aka a wall).

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | fromX          |                |         |
| 2     | fromY          |                |         |
| 2     | toX            |                |         |
| 2     | toY            |                |         |
| 2     | H              |                |         |
| 2     | CH             |                |         |
| 2     | duration       |                |         |
| 2     | collision_velocity         |                |         |
| 2     | toY            |                |         |

## 0x12: F_PLAYER_SHORT_STATE

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | ownerId        |                |         |
| 1     |                |                |         |
| 1     |                |                |         |
| 1     |                |                |         |
| 1     |                |                |         |
| 1     |                |                |         |

## 0x13: F_DAMAGE

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | who            | debug log      | -5: Bleeding<br>-30: On fire<br>-34: Suffocating<br>< 0: Damage<br>0: Self?<br>> 0: Player ID |
| 1     | damage         | debug log      |         |
| 1     | power          | debug log      | Power drain |
| 1     | hits           | debug log      |         |

## 0x14: Hit_Player

Info from the server, that the current player hit someone.

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | playerId       |                | If playerId equals the current players ID, the player hit himself |

## 0x15: Hit_NPC

Confirmation from the server, that the current player hit an NPC.

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | npcId(?)       |                | unused  |

## 0x1e: GET_FLAG_RESPONSE

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | who            | debug log      |         |
| 2     | which          | debug log      |         |

## 0x20: DROP_FLAG_RESPONSE

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | who            | debug log      |         |

## 0x21: F_PLAYER_CLEAR_SPLAT

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 2     | who            |                | ID of the acting player |
| 2     | playerId       |                | -2: Tap power and resurrect<br>-1: Nexus has been destroyed<br>0: Send F_PLAYER_STATE to server<br> > 0: Raise player |

## 0x25: CREATE_EFFECT

Create an effect from a spell.

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 1     | type           | debug log      |         |
| 1     |                |                |         |
| 1     | ?              |                | 4: Effect was cast on a player?<br>2: Effect was cast on an object? |
| 1     |                |                |         |
| 2     | source         |                |         |
| 2     | target         |                | Player of the effect recipient |
| 2     | range          | debug log      |         |
| 2     | creator?       |                |         |
| 2     | x              |                |         |
| 2     | y              |                |         |
| 2     | z              |                |         |
| 2     | posX           |                |         |
| 2     | posY           |                |         |
| 2     | posZ           |                |         |
| 1     | target_again?  |                |         |
| 1     | team           |                |         |
| 1     | element        |                |         |
| 1     | ?              |                |         |
| 2     | ?              |                |         |
| 2     | spell          | debug log      | Must not be 0 |

## 0x27: F_PLAYER_EFFECT

## 0x29: EARTHBLOOD_STATE

| bytes | type           | source         | remarks |
|-------|----------------|----------------|---------|
| 1     | which          | debug log      | Array index of the earthbloodstate (not an ID!) |
| 1     | team           | debug log      | Called "align" in the debug log |
| 1     | bias           | debug log      |         |
| 1     | (unused)       |                |         |
| 1     | amount         | debug log      | 0: Attempt failed<br> > 99: Fully biased: <br> < -99: Eliminate bias on earthnode |
| 1     | power          |                |         |
| 2     | biaser         | debug log      |         |

# References

## Object type

| type          | value | remarks |
|---------------|-------|---------|
| Static object | 0     | |
| Projectile    | 1     | Potentially "Movable object"              |
| Mongen        | 2     | AKA monster, part of the engine but likely unused in Spellbinder |
| Unknown3      | 3     | Potentially "Rune" |
| Player        | 4     | |
| Unknown5      | 5     | |
| Unknown7      | 7     | |
| Spirit gate   | 8     | |

## Team

| type     | value |
|----------|-------|
| None     | 0     |
| Dragon   | 1     |
| Gryphon  | 2     |
| Phoenix  | 3     |

# Abbreviations

| Name  | Long name   | Explanation                                          |
|-------|-------------|------------------------------------------------------|
| OLIST | Object list | List of game objects known to the client (max. 1000) |