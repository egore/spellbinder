extends Node
class_name Spell

# The minimum level a player must have to use the spell
@export var minimumLevel: int = 1

# Cooldown a spell has (so we don't fire each frame)
@export var cooldown: float = 1.0

# The projectile that is created from the spell (the spell does not
# have an immediate effect, but the projectile will cause the effect,
# e.g. damage)
@export var projectile: PackedScene

# The immediate effect caused by this spell (only used if no projectile is specified)
@export var effect: NodePath

# The amount of health casting this spell costs the caster
@export var healthCost: int

# The amount of fatigue casting this spell costs the caster
@export var fatigueCost: int = 10

var camera

func _ready():
	camera = get_node("/root/Level/Avatar/Main Camera3D")

func cast(origin, ray: RayCast3D) -> void:
	# The spell does not cause an immediate effect, but rather a projectile
	if projectile:
		# Instantiate the game object at the camera position plus 'a bit forward'
		var projectileInstance = projectile.instantiate()
		get_tree().get_root().add_child(projectileInstance)
		projectileInstance.global_transform = camera.global_transform
		projectileInstance.shoot()
		return

	# The spell causes a direct effect checked the targetted player
	if effect:
		if ray.is_colliding():
			var opponent = ray.get_collider()
			if opponent.is_in_group("Player"):
				opponent.health.applyEffect(get_node(effect))
		return
