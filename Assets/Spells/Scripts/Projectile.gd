extends RigidBody3D
class_name Projectile

@export var speed: float = 50.0
@export var damage: int = 20

var is_shooting := false

func shoot():
	print("Firing projectile")
	is_shooting = true

func _physics_process(delta):
	if is_shooting:
		apply_impulse(-transform.basis.z * speed, transform.basis.z)

func _on_Area_area_entered(area):
	print("Projectile hit entity")
	if area.is_in_group("Player"):
		area.health.doDamage(damage)
	# TODO add desolving effect
	# Destroy on any impact
	sleeping = true
	queue_free()
