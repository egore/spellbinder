# UI component to display list of players per team
extends Label

# All teams in the current level
var teams

func _ready():
	teams = get_tree().get_nodes_in_group("Team")
	
	var scn = preload("res://Assets/Ui/Team player list.tscn")

	for team in teams:
		var o = scn.instantiate()
		self.add_child(o)
		o.name = "Team player list %s" % team.name
		o.init(team)
		team.connect("player_joined", handle_player_joined)
	repositionChildren()

func repositionChildren():
	var top = 16.0
	var i = 0
	for team in teams:
		var o: Control = get_child(i)
		o.offset_top = top
		top += 16.0 * (team.players.size() + 3)
		i += 1

func handle_player_joined(_team) -> void:
	var players = 0
	for team in teams:
		players += team.players.size()
	text = "%d Players" % players
	repositionChildren()
