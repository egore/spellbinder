extends TextureProgressBar

func _ready():
	var health = get_node("/root/Level/Avatar/Health")
	health.connect("health_changed", handle_health_changed)
	self.min_value = 0
	self.max_value = health.maxHealth

func handle_health_changed(currentHealth):
	value = currentHealth
