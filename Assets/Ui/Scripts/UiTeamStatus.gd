extends Node

var nexus: Nexus

func getNexus(team: Team) -> Nexus:
	var nexi = get_tree().get_nodes_in_group("Nexus")
	for nexus_ in nexi:
		if nexus_.owningTeam == team && nexus_.currentTeam == team:
			return nexus_
	return null

func _ready():

	var team: Team
	if name == "Team Status Dragon":
		team = get_node("/root/Level/Teams/Team Dragon")
	elif name == "Team Status Gryphon":
		team = get_node("/root/Level/Teams/Team Gryphon")
	elif name == "Team Status Phoenix":
		team = get_node("/root/Level/Teams/Team Phoenix")
	else:
		queue_free()
		return

	self.nexus = getNexus(team)

	self.min_value = 0
	self.max_value = nexus.maxHealth

func _process(_delta):
	if nexus.health > 0.0 && nexus.owningTeam == nexus.currentTeam:
		self.visible = true
		self.value = nexus.health
	else:
		self.visible = false

