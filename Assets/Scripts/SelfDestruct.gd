using UnityEngine;

/// <summary>
/// Destroy a game object after a given time
/// </summary>
public class SelfDestruct : MonoBehaviour
{

	public float duration = 1.0f;

	void Update()
	{
		duration -= Time.deltaTime;
		if (duration <= 0.0f)
		{
			Destroy(gameObject);
		}
	}
}
