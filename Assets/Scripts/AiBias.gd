# The player is biasing a nexus / earthnode.
extends Node
class_name AiBias

@export var amount: float = 5.0

var nexus: Nexus : get = nexus_get, set = nexus_set

var biasParticles: GPUParticles3D

func _ready():
	biasParticles = get_parent().get_node("GPUParticles3D")

func _process(delta):
	if nexus:
		# When the bot is within range of a nexus he wants to bias it
		var playerTeam = get_parent().get_node("player").team
		if nexus.currentTeam != playerTeam:
			if !biasParticles.visible:
				biasParticles.process_material.color = playerTeam.color
				biasParticles.visible = true
			nexus.bias(delta * amount, playerTeam)
		else:
			biasParticles.visible = false
	else:
		biasParticles.visible = false

func nexus_get() -> Nexus:
	return nexus

func nexus_set(other: Nexus) -> void:
	nexus = other
	biasParticles.visible = !!other

