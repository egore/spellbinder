extends Node
class_name Profession

@export var availableSpells: Array[NodePath] = []

@onready var shortName = ("%s" % name).substr(0, 2)
