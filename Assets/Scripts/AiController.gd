extends Node
class_name AiController

var target

var nav
@onready var textMesh = $MeshInstance3D
var anim
var ray

var bias
var health
var player
var aiCastSpell
var label

func FindTarget():
	# Choose a random target (will be done prior to instantiating it to not make it target itself)
	var players = get_tree().get_nodes_in_group("Player")
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	for i in range(0, 5):
		target = players[rng.randi_range(0, players.size() - 1)]
		if target != self:
			return
	# No target yet, destroy the bot
	queue_free()

func _ready():
	anim = $player/AnimationPlayer
	bias = $Bias
	health = $Health
	player = $player
	aiCastSpell = $"Ai Cast Spell"
	label = $SubViewport/Label
	FindTarget()

func _process(_delta):
	# TODO hide name if player not hitable by ray
	#textMesh.transform.LookAt(Camera3D.main.transform.position)

	# No target, idle around
	if !target:
		nav.enabled = false
		anim.current_animation = "Standing"
		FindTarget()
		return

	if self.position.distance_to(target.position) > 1.5:
		nav.enabled = true
		anim.current_animation = "Running"
		nav.SetDestination(target.position)
	else:
		nav.enabled = false
		anim.current_animation = "Standing"
