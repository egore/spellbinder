extends Node
class_name Team

@export var color: Color
@export var nexusImage: CompressedTexture2D
var players = []

signal player_joined(team: Team)

func leave(player: Player) -> void:
	var index = players.find(player)
	if index >= 0:
		players.remove_at(index)

func join(player: Player) -> void:
	var index = players.find(player)
	if index == -1:
		players.push_back(player)
		emit_signal("player_joined", self)
