extends Node3D
class_name Nexus

# A teams nexus (or the smaller earth nodes).

@export var owningTeam: Team
@export var maxHealth: float = 100.0
var icosphere: MeshInstance3D
var defaultIcosphereMaterial
var health
var s
@onready var currentTeam: Team = owningTeam

func _ready():
	# set up health to be the max health
	health = maxHealth

	# Get ahold of our particle system and icosphere
	icosphere = $Model/Icosphere
	s = $GPUParticles3D

	# and initialize it to the team
	currentTeam = owningTeam
	var color = currentTeam.color
	s.process_material.color = color
	icosphere.material_override = StandardMaterial3D.new()
	icosphere.material_override.albedo_color = color
	$OmniLight3D.light_color = color

func _process(delta):
	icosphere.rotation.y += delta
	icosphere.rotation.x += 0.75 * delta

func bias(amount, team):

	# Nexus / Earthnode has no health, we will change the color first ...
	if health == 0.0:
		currentTeam = team
		s.process_material.color = currentTeam.color
		icosphere.material_override.albedo_color = currentTeam.color

	# ... and then increase (or decrease) the health ...
	if currentTeam == team:
		health = min(health + amount, maxHealth)
	else:
		health = max(health - amount, 0.0)

	# ... and if no health is left, reset the color
	if health == 0.0:
		currentTeam = null
		s.process_material.color = Color(1, 1, 1)
		icosphere.material_override.albedo_color = Color(1, 1, 1)

func _on_Area_body_entered(body):
	# When the player enters a nexus, remember it
	if body.is_in_group("Player"):
		body.bias.nexus = self


func _on_Area_body_exited(body):
	# When the player leaves a nexus, forget about it
	if body.is_in_group("Player"):
		body.bias.nexus = null
