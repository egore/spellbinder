# Describes a player in the game. This could be either the avatar or any other player.
extends Node3D
class_name Player

# The experience level the player has
@export var level: int = 1

# The profession the player has
var profession: Profession

# The team the player belogins to
var team: Team : get = team_get, set = team_set

var playername: String = "unnamed"

func free():
	team_set(null)

func team_set(newteam: Team) -> void:
	if team:
		team.leave(self)
	team = newteam
	if team:
		team.join(self)

func team_get() -> Team:
	return team
